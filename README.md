# Development Setup

## Installation
```
pip install -e git+git@bitbucket.org:imfreemobile/okspinoy.git@0.4#egg=okspinoy-client
```

## Usage:
### Balance Inquiry
```python
from okspinoy import OksPinoy

oks = OksPinoy(host='www.okspinoy.com', port=443, username='<username>', password='<password>', uri="/partners/soap/index.php")
resp = oks.execute.get_wallet_balance('--account--no--')
print resp
```

### Topup or send load
```
from okspinoy import OksPinoy

oks = OksPinoy(host='www.okspinoy.com', port=443, username='<username>', password='<password>', uri="/partners/soap/index.php")
resp = oks.execute.topup_and_tag('0915XXXXXXX', '8000000046851')
print resp
```

### Get Transaction History
```
from okspinoy import OksPinoy

oks = OksPinoy(host='www.okspinoy.com', port=443, username='<username>', password='<password>', uri="/partners/soap/index.php")
resp = = oks.execute.get_history('2017-03-24')
print resp
```

### Get Transaction Details
```
from okspinoy import OksPinoy

oks = OksPinoy(host='www.okspinoy.com', port=443, username='<username>', password='<password>', uri="/partners/soap/index.php")
resp = = oks.execute.get_trans_details('123456789')
print resp
```
