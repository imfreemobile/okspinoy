from .connection import RequestsHttpConnection
from .helpers.parser import SOAPXMLToDictParser
from .exceptions import *
from .config import *


class Transport(object):
    def __init__(self, host, port, connection_class=RequestsHttpConnection, default_parser=SOAPXMLToDictParser(),
                 default_mimetype='text/xml', max_retries=3, retry_on_status=(502, 503, 504), retry_on_timeout=False,
                 send_get_body_as='GET', **kwargs):
        """

        :param host:
        :param port:
        :param connection_class:
        :param default_parser:
        :param default_mimetype:
        :param max_retries:
        :param retry_on_status:
        :param retry_on_timeout:
        :param send_get_body_as:
        :param kwargs:
        :return:
        """

        self.max_retries = max_retries
        self.retry_on_timeout = retry_on_timeout
        self.retry_on_status = retry_on_status
        self.send_get_body_as = send_get_body_as
        self.default_mimetype = default_mimetype

        self.kwargs = kwargs
        self.host = host
        self.port = port
        self.parser = default_parser

        self.connection_class = connection_class
        self.connection = self.connection_class(host=self.host, port=self.port, **kwargs)

    def perform_request(self, method, uri, params=None, body=None, headers=None):
        """

        :param method:
        :param uri:
        :param params:
        :param body:
        :return:
        """

        if body is not None:
            body = self.parser.dumps(body)

            # some clients or environments don't support sending GET with body
            if method in ('HEAD', 'GET') and self.send_get_body_as != 'GET':
                # send it as post instead
                if self.send_get_body_as == 'POST':
                    method = 'POST'

                # or as source parameter
                elif self.send_get_body_as == 'source':
                    if params is None:
                        params = {}
                    params['source'] = body
                    body = None
        logger.debug((self, method, uri, params, headers, body))

        if body is not None:
            try:
                body = body.encode('utf-8')
            except (UnicodeDecodeError, AttributeError):
                # bytes/str - no need to re-encode
                pass


        ignore = ()
        timeout = None
        if params:
            timeout = params.pop('request_timeout', None)
            ignore = params.pop('ignore', ())
            if isinstance(ignore, int):
                ignore = (ignore,)

        for attempt in range(self.max_retries + 1):
            connection = self.connection

            try:
                status, headers, data = connection.perform_request(
                    method, uri, params, body, headers=headers,
                    ignore=ignore, timeout=timeout
                )

            except TransportError as e:
                if method == 'HEAD' and e.status_code == 404:
                    return False

                retry = False
                if isinstance(e, ConnectionTimeout):
                    retry = self.retry_on_timeout
                elif isinstance(e, ConnectionError):
                    retry = True
                elif e.status_code in self.retry_on_status:
                    retry = True

                if retry:
                    # raise exception on last retry
                    if attempt == self.max_retries:
                        raise
                else:
                    raise

            else:
                if method == 'HEAD':
                    return 200 <= status < 300

                # connection didn't fail, confirm it's live status
                if data:
                    logger.debug((data, headers, self.default_mimetype))
                    data = self.parser.loads(data, headers.get('content-type', self.default_mimetype))
                return data

    def close(self):
        """
        Explicitly closes connections
        """
        self.connection.close()
