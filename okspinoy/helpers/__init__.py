from ..constants import *
import copy
from datetime import datetime

def xml_escape(data):
    # code to escape data
    return data


def soap_envelope(xml_str):
    soap_env = copy.deepcopy(SOAP_ENVELOPE).replace("\n", "")
    return soap_env.format(
        body=xml_escape(xml_str)
    )


def generate_merchant_trans_id(merchant_name='okspinoy'):
    return "imfree{}".format(datetime.now().strftime('%Y%m%d%H%M%S'))


def get_encrypted_password(username, password, session_id):
    step1_str = "{}{}{}".format(username, password, session_id)
    final_str = hashlib.sha1(step1_str.encode('utf-8')).hexdigest()
    return final_str


def is_valid_sku(sku):
    if sku in SKU_LIST and SKU_LIST[sku]:
        return True


def get_sku_val(sku):
    if is_valid_sku(sku):
        return SKU_LIST[sku]

from parser import *