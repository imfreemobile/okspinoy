string_types = str, bytes
from . import *
from ..exceptions import ParseError, ConfigurationError
import re

class TextParser(object):
    mimetype = 'text/plain'

    def loads(self, s, mimetype= 'text/xml'):
        return s

    def dumps(self, data):
        if isinstance(data, string_types):
            return data

        raise ParseError('Cannot serialize %r into text.' % data)


class XMLToDictParser(object):
    mimetype = 'text/xml'
    valid_mimetypes = [
        'text/html',
        'application/xml',
        'application/soap'
        'application/wsdl'
    ]

    def loads(self, s, mimetype= 'text/xml'):
        try:
            logger.debug((self, sys._getframe().f_code.co_name, s))
            return xmltodict.parse(s)
        except (ValueError, TypeError) as e:
            raise ParseError(s, e)

    def dumps(self, data):
        # don't serialize strings
        if isinstance(data, string_types):
            return data

        try:
            xml_doc = xmltodict.unparse(xml_escape(data), pretty=True)
            logger.debug((self, sys._getframe().f_code.co_name, xml_doc))
            return xml_doc
        except (ValueError, TypeError) as e:
            raise ParseError(data, e)


class SOAPXMLToDictParser(object):
    mimetype = 'text/xml'
    valid_mimetypes = [
        'text/xml',
        'text/html',
        'application/xml',
        'application/soap'
        'application/wsdl'
    ]

    def _is_valid_mimetype(self, mimetype):
        if mimetype != self.mimetype:
            if mimetype in self.valid_mimetypes:
                return True
            else:
                for mtype in self.valid_mimetypes:
                    if re.search(mtype, mimetype):
                        return True
        else:
            return True

    def loads(self, s, mimetype= 'text/xml'):
        """

        :param s:
        :param mimetype:
        :return:
        """
        if not self._is_valid_mimetype(mimetype):
            raise ParseError(s, "content-type:%s not parsable" % mimetype)

        try:
            logger.debug((self, sys._getframe().f_code.co_name, s))
            response_data = xmltodict.parse(s)

        except (ValueError, TypeError) as e:
            raise ParseError(s, e)

        else:
            return response_data

    def dumps(self, data):
        """
        dict to XML str in soap envelope

        :param data:
        :return:
        """
        if isinstance(data, string_types):
            return soap_envelope(data).rstrip()

        try:
            xml_doc = soap_envelope(xmltodict.unparse(data, full_document=False)).rstrip("\n")
            logger.debug((self, sys._getframe().f_code.co_name, xml_doc))
            return xml_doc
        except (ValueError, TypeError) as e:
            raise ParseError(data, e)

DEFAULT_PARSERS = {
    XMLToDictParser.mimetype: XMLToDictParser(),
    TextParser.mimetype: TextParser(),
    SOAPXMLToDictParser.mimetype: SOAPXMLToDictParser(),
}
