__all__ = [
    'ConfigurationError',
    'OksPinoyException',
    'ParseError',
    'XMLParseError',
    'SOAPParseError',
    'TransportError',
    'ConnectionError',
    'ConnectionTimeout',
    'NotFoundError',
    'ConflictError',
    'RequestError',
    'AuthenticationError',
    'AuthorizationError',
    'SSLError'
]


class ConfigurationError(Exception):
    """
    Raised when there's a configuration missing or wrong
    """


class OksPinoyException(Exception):
    """
    Base class for all OksPinoy exceptions
    """


class ParseError(OksPinoyException):
    """
    Error raised when request data cannot be serialized
    """


class SOAPParseError(ParseError):
    """
    Error raised when SOAP response data cannot be parsed
    """


class XMLParseError(ParseError):
    """
    Error raised when XML response data cannot be parsed
    """


class TransportError(OksPinoyException):
    """
    Exception raised when OksPinoy API returns a non-OK (>=400) HTTP status code. Or when
    an actual connection error happens; in that case the ``status_code`` will
    be set to ``'N/A'``.
    """
    @property
    def status_code(self):
        """
        The HTTP status code of the response that precipitated the error or
        ``'N/A'`` if not applicable.
        """
        return self.args[0]

    @property
    def error(self):
        """ A string error message. """
        return self.args[1]

    @property
    def info(self):
        """ Dict of returned error info from ES, where available. """
        return self.args[2]

    def __str__(self):
        cause = ''
        # try:
        #     if self.info:
        #         cause = ', %s' % self.info['error']['root_cause'][0]['reason']
        # except LookupError:
        #     pass
        return 'TransportError(%s, %r%s)' % (self.status_code, self.error, cause)


class ConnectionError(TransportError):
    """
    Raised when there's connection problem while transacting with OksPinoy API
    """


class SSLError(TransportError):
    """
    Raised when there's SSL/TLS transaction error
    """


class ConnectionTimeout(TransportError):
    """
    Raised when the current connection / request times out
    """


class NotFoundError(TransportError):
    """ Exception representing a 404 status code. """


class ConflictError(TransportError):
    """ Exception representing a 409 status code. """


class RequestError(TransportError):
    """ Exception representing a 400 status code. """


class AuthenticationError(TransportError):
    """ Exception representing a 401 status code. """


class AuthorizationError(TransportError):
    """ Exception representing a 403 status code. """


HTTP_EXCEPTIONS = {
    400: RequestError,
    401: AuthenticationError,
    403: AuthorizationError,
    404: NotFoundError,
    409: ConflictError,
}


