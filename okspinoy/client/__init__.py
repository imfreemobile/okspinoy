#!/bin/venv python
# -*- coding: utf-8 -*-
# okspinoy/client/__init__.py
import copy
import logging
class NamespacedClient(object):
    def __init__(self, client):
        self.client = client

    @property
    def transport(self):
        return self.client.transport

from .session import *
from .execute import *
from ..constants import *
from ..transport import Transport

use_ssl = True if OKSPINOY_SCHEME=='https' else False


class OksPinoy(object):
    def __init__(self, host=OKSPINOY_HOSTNAME, port=OKSPINOY_PORT, transport_class=Transport, use_ssl=use_ssl, **kwargs):
        """

        :param host:
        :param port:
        :param transport_class:
        :param kwargs: if uri is given, it will only
        :return:
        """
        self.transport = transport_class(host=host, port=port, use_ssl=use_ssl, **kwargs)
        self.session = SessionClient(self, username=kwargs.get(USERNAME, None), uri=kwargs.get(URI, ""))

        session_data = None
        if kwargs.get(USERNAME, None):
            session_data = self.session.create(uri=kwargs.get(URI, ""))

        self.execute = ExecuteClient(self,
                                     username=kwargs.get(USERNAME, None),
                                     password=kwargs.get(PASSWORD, None),
                                     session_id=session_data[SESSION_ID] if session_data else None
                                     )

        self.session_id = session_data[SESSION_ID] if session_data else None
        self._username = kwargs.get(USERNAME, None)
        self._password = kwargs.get(PASSWORD, None)

    @property
    def hash_pass(self):
        try:
            return self.execute.hash_pass
        except Exception as e:
            audit_logger.warn((self, sys._getframe().f_code.co_name, e, traceback.print_exc()))

    def __repr__(self):
        return "<OksPinoy(%s:%s session_id:%s, username: %s)>" \
               % (self.transport.host, self.transport.port, self.session_id, self._username)


class OksPinoyResponse():
    def __init__(self):
        pass
