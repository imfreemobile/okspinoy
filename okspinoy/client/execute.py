# okspinoy/client/execute.py
from . import *
from ..helpers import *
import cgi
from datetime import datetime
from collections import OrderedDict
from six import iteritems

logging.basicConfig(format='{%(filename)s:%(lineno)d} %(levelname)s - %(message)s')
audit_logger = logging.getLogger('OksPinoy_auditlog')
audit_logger.setLevel(logging.WARNING)


def execute_command_data_xmldoc(data):
    data_stanza = copy.deepcopy(OKSPINOY_EXECUTE_CMD_DATA_TPL).replace("\n","")
    xml_params = ""
    for key, val in iteritems(data):
        xml_params += "<%s>%s</%s>\n" % (key, val, key)
    data_stanza_xmldoc = data_stanza.format(xml_params=xml_params)
    return data_stanza_xmldoc


def execute_command_xmldoc(session_id, username, password, command, data):
    """
    :param session_id:
    :param username:
    :param password: OksPinoy password hashing algorithm - sha1(username+password+session_id)
    :param command:
    :param data: WARNING: this is an escaped xml doc
    :return:
    """
    execute_stanza = copy.deepcopy(OKSPINOY_EXECUTE_CMD_TPL).replace("\n", "")
    data = execute_command_data_xmldoc(data=data)
    execute_stanza = execute_stanza.format(
        session_id=session_id,
        username=username,
        password=password,
        command=command,
        data=cgi.escape(data)
    )
    return execute_stanza


class ExecuteClient(NamespacedClient):
    def __init__(self, client, **kwargs):
        super(ExecuteClient, self).__init__(client=client)
        self.commands = [i for i in OKSPINOY_EXECUTE_COMMANDS]
        self.client = client
        self._username = kwargs.get(USERNAME, None)
        self._password = kwargs.get(PASSWORD, None)
        # last called command
        self.command = kwargs.get(COMMAND, None)
        # last payload
        self.data = kwargs.get(DATA, None)
        self.uri = kwargs.get(URI, None)

        self.raw = None

    @property
    def session_id(self):
        return self.client.session.create(username=self._username)[SESSION_ID]

    def hash_pass(self, session_id):
        return get_encrypted_password(self._username, self._password, session_id or self.session_id)

    def _execute(self, username, password, command, data, uri=OKSPINOY_EXECUTE_URI, **kwargs):

        if command not in self.commands:
            logger.warn((self, sys._getframe().f_code.co_name, "Command:%s not found" % command))
            return False
        session_id = self.session_id
        self._username = username or self._username
        self._password = password or self._password
        self.command = command
        self.data = data
        self.uri = uri

        request_data = execute_command_xmldoc(
            session_id=session_id,
            username=username,
            password=self.hash_pass(session_id),
            command=command,
            data=data
        )
        audit_logger.warn((self, sys._getframe().f_code.co_name, session_id, username, password, uri, request_data))

        if kwargs.get("debug", False):
            parser_context = SOAPXMLToDictParser()
            enveloped_data = parser_context.dumps(request_data)
            logger.debug((self, sys._getframe().f_code.co_name, enveloped_data))
            return enveloped_data

        response_data = self.transport.perform_request(
            'POST',
            uri=uri,
            params=kwargs,
            body=request_data
        )

        if response_data:
            audit_logger.warn((self, sys._getframe().f_code.co_name,
                               session_id,
                               username,
                               password,
                               uri,
                               request_data,
                               response_data
                               ))
            logger.debug((self, sys._getframe().f_code.co_name, response_data))
            return response_data[SOAP_ENV_MAIN_TAG][SOAP_ENV_BODY_TAG][OKSPINOY_EXECUTE_RESPONSE]

    def get_history(self, trans_date, command=TOPUPANDTAG, offset=0, username=None, password=None, uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        Gets the last 30 transaction records and their status.

        :param trans_date:
        :param offset:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """
        data = OrderedDict()
        data[DATE] = trans_date
        data[OFFSET] = offset
        data[COMMAND] = command

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=GETHISTORY,
            data=data,
            uri=uri,
            **kwargs
        )
        return response_data

    def get_trans_details(self, trans_id, username=None, password=None, uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This gets the status of a transaction queried by the transaction id received.

        :param trans_id:
        :param session_id:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """
        data = {
            TRANSACTION_ID: trans_id
        }
        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=GETRANSDETAILS,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def get_trans_details_by_merchant_id(self, merchant_trans_id, username=None, password=None,
                                         uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This gets the status of a transaction queried by the merchant transaction id.

        :param merchant_trans_id:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """
        data = {
            MERCHANT_TRANSACTION_ID: merchant_trans_id
        }
        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=GETTRANSDETAILSBYMERCHANTID,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def get_wallet_balance(self, account_no=None, username=None, password=None, uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This gets the balance of a wallet associated with a merchant.

        :param account_no: Account No of the wallet of a merchant. If left blank, will return all wallet
                           associated with the merchant.
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """
        data = OrderedDict()
        data[ACCOUNT_NO] = account_no

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=GETWALLETBALANCE,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def topup(self, mobile_no, sku, merchant_trans_id=generate_merchant_trans_id(),
              username=None, password=None, uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This top ups an electronic load.

        :param merchant_trans_id:
        :param merchant_date:
        :param mobile_no:
        :param sku:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """

        if not is_valid_sku(sku):
            return False

        merchant_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        data = OrderedDict()
        data[MERCHANT_TRANSACTION_ID] = merchant_trans_id
        data[MERCHANT_DATE] = merchant_date
        data[MOBILE_NO] = mobile_no
        data[SKU] = sku

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=TOPUP,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def topup_and_tag(self, mobile_no, sku, group="sys", merchant_trans_id=generate_merchant_trans_id(),
                      username=None, password=None, uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This topups an electronic load and tags a specific transaction to a certain group.

        :param merchant_date:
        :param mobile_no:
        :param sku:
        :param group:
        :param merchant_trans_id:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """

        if not is_valid_sku(sku):
            return False

        merchant_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        data = OrderedDict()
        data[MERCHANT_TRANSACTION_ID] = merchant_trans_id
        data[GROUP] = group
        data[MERCHANT_DATE] = merchant_date
        data[MOBILE_NO] = mobile_no
        data[SKU] = sku

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=TOPUPANDTAG,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def topup_username(self, sku, other_username, merchant_trans_id=generate_merchant_trans_id(),
                       username=None, password=None, uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This top ups a third-party account identified by a username.

        :param merchant_trans_id:
        :param merchant_date:
        :param sku:
        :param other_username:
        :param session_id:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """

        if not is_valid_sku(sku):
            return False

        merchant_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        data = OrderedDict()
        data[MERCHANT_TRANSACTION_ID] = merchant_trans_id
        data[MERCHANT_DATE] = merchant_date
        data[SKU] = sku
        data[USERNAME] = other_username

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=TOPUPUSERNAME,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def topup_with_sms(self, **kwargs):
        """
        This top ups with an SMS verification.

        :param data:
        :param session_id:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """

        raise NotImplementedError('Boom!')

    def activate_coupon(self, merchant_trans_id, coupon, username=None, password=None,
                        uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        Activates the coupon.

        :param merchant_trans_id:
        :param coupon:
        :param session_id:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """

        data = OrderedDict()
        data[MERCHANT_TRANSACTION_ID] = merchant_trans_id
        data[COUPON] = coupon

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=ACTIVATECOUPON,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data

    def coupon_status(self, merchant_trans_id, coupon, username=None, password=None,
                        uri=OKSPINOY_EXECUTE_URI, **kwargs):
        """
        This command will query the status of a coupon.

        :param merchant_trans_id:
        :param coupon:
        :param username:
        :param password:
        :param uri:
        :param kwargs:
        :return:
        """

        data = OrderedDict()
        data[MERCHANT_TRANSACTION_ID] = merchant_trans_id
        data[COUPON] = coupon

        response_data = self._execute(
            username=username if username else self._username,
            password=password if password else self._password,
            command=COUPONSTATUS,
            data=data,
            uri=uri,
            **kwargs
        )

        return response_data