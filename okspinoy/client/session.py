# okspinoy/client/session.py
from . import *
from ..helpers import *


def create_session_data(username):
    cs_dict = copy.deepcopy(OKSPINOY_CREATE_SESSION_DICT_TPL)
    cs_dict["CreateSession"]["username"] = username
    return cs_dict


class SessionClient(NamespacedClient):
    def __init__(self, client, username=None, **params):
        super(SessionClient, self).__init__(client=client)
        if username:
            self.username = username

    def create(self, username=None, uri=OKSPINOY_CREATE_SESSION_URI, **params):
        """
        Create a session on OksPinoy

        :param username:
        :param uri:
        :param params:
        :return:
        """
        if not username and self.username:
            username = self.username

        data = create_session_data(username)

        if params.get("debug", False):
            parser_context = SOAPXMLToDictParser()
            enveloped_data = parser_context.dumps(data)
            logger.debug((self, sys._getframe().f_code.co_name, enveloped_data))
            return enveloped_data

        response_data = self.transport.perform_request(
            'POST',
            uri=uri,
            params=params,
            body=data,
        )
        if response_data:
            logger.debug((self, sys._getframe().f_code.co_name, response_data))
            return response_data[SOAP_ENV_MAIN_TAG][SOAP_ENV_BODY_TAG][OKSPINOY_CREATE_SESSION_RESPONSE]
