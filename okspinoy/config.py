#!/bin/venv python
# -*- coding:utf-8 -*-
# config.py
import logging

import environ

env = environ.Env()

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

OKSPINOY_SCHEME = "https"
OKSPINOY_HOSTNAME = "www.okspinoy.com"
OKSPINOY_PORT = 443
OKSPINOY_WSDL_URL = "https://www.okspinoy.com/partners/soap/?wsdl"
OKSPINOY_CREATE_SESSION_URI = "/partners/soap/index.php"
OKSPINOY_EXECUTE_URI = "/partners/soap/index.php"
OKSPINOY_PRIMARY_URI = "/partners/soap/index.php"
OKSPINOY_RETAILER_USERNAME = env('OKSPINOY_RETAILER_USERNAME', default='')
OKSPINOY_RETAILER_PASSWORD = env('OKSPINOY_RETAILER_PASSWORD', default='')
OKSPINOY_ACCT_NO = env('OKSPINOY_ACCT_NO', default='')
SKU_LIST = {
    "8000000108205": 5,
    "8000000046851": 10,
    "8000000013792": 15,
    "8000000013865": 20,
    "8000000013947": 30,
    "8000000014032": 40,
    "8000000014091": 50,
    "8000000014143": 60,
    "8000000014176": 70,
    "8000000014215": 80,
    "8000000014257": 90,
    "8000000014267": 100,
    "8000000014307": 110,
    "8000000014493": 115,
    "8000000014321": 120,
    "8000000014343": 130,
    "8000000014356": 140,
    "8000000014409": 150,
    "8000000011570": 500
}
