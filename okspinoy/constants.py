from . import *
RAW = 'raw'
BODY = 'body'
URI = 'uri'

RESULT_CODE = 'resultCode'
SESSION_ID = 'sessionId'
RESULT_MESSAGE = 'resultMessage'
USERNAME = 'username'
PASSWORD = 'password'
COMMAND = 'command'
DATA = 'data'
TRANSACTION_ID = 'transactionId'
MERCHANT_TRANSACTION_ID = 'merchantTransactionId'
MERCHANT_DATE = 'merchantDate'
MOBILE_NO = 'mobileNo'
SKU = 'sku'
GROUP = 'group'
RETURN = 'return'
META = 'meta'
DATE = 'date'
OFFSET = 'offset'
ACCOUNT_NO = 'accountNo'
COUPON = 'coupon'

TOTAL_RECORDS = "totalRecords"
RECORDS = "records"
ITEM = "item"
STATUS_CODE = "statusCode"
WALLET = "wallet"
BALANCE = "balance"
PRICE = "price"
TOPUP_REF_NO = "topupReferenceNo"

XMLTODICT_XMLNS = "@xmlns"
XMLTODICT_TEXT = "#text"
XMLTODICT_DICT = "#dict"

RESULT_CODES = {
    "CreateSession": {
        1: "Cannot create session id",
        2: "Success. Request received and processed.",
        3: "Invalid xml request format.",
        4: "Missing/incomplete request parameter(s)",
        5: "Username not registered",
        6: "Unauthorized IP"
    },
    "Execute": {
        1: "Internal error",
        2: "Request received and processed.",
        3: "Invalid xml request format.",
        4: "Missing/incomplete request parameter(s)",
        5: "Message too long",
        6: "Service temporarily unavailable",
        7: "Invalid username/password. Failed authentication.",
        8: "Expired session/invalid session Id",
        9: "Session Id does not exist.",
        10: "Insufficient balance",
        11: "Invalid destination",
        12: "Invalid source",
        13: "Invalid message format",
        14: "Command not supported",
        15: "Invalid Merchant Date",
        16: "Merchant Transaction Id too long",
        17: "No Price set for Command",
        18: "No exchange rate for currency",
        19: "Wallet Account not Active",
        20: "Sku not supported",
        21: "Beneficiary name not set",
        22: "Beneficiary name too long",
        23: "Coupon does not exist",
        24: "Coupon has already been claimed",
        25: "Merchant Transaction Id does not exist",
        26: "Transaction Id does not exist",
        27: "Wallet Account does not exist",
        28: "Duplicate Merchant Transaction Id",
        29: "No Merchant Transaction Id Supplied"
    }
}

GETHISTORY = "GETHISTORY"
GETRANSDETAILS = "GETTRANSDETAILS"
GETTRANSDETAILSBYMERCHANTID = "GETTRANSDETAILSBYMERCHANTID"
GETWALLETBALANCE = "GETWALLETBALANCE"
TOPUP = "TOPUP"
TOPUPANDTAG = "TOPUPANDTAG"
TOPUPUSERNAME = "TOPUPUSERNAME"
TOPUPWITHSMS = "TOPUPWITHSMS"
ACTIVATECOUPON = "ACTIVATECOUPON"
COUPONSTATUS = "COUPONSTATUS"


OKSPINOY_EXECUTE_COMMANDS = [
    GETHISTORY,
    GETRANSDETAILS,
    GETTRANSDETAILSBYMERCHANTID,
    GETWALLETBALANCE,
    TOPUP,
    TOPUPANDTAG,
    TOPUPUSERNAME,
    TOPUPWITHSMS,
    ACTIVATECOUPON,
    COUPONSTATUS
]

SOAP_ENV_MAIN_TAG = 'SOAP-ENV:Envelope'
SOAP_ENV_BODY_TAG = 'SOAP-ENV:Body'

OKSPINOY_CREATE_SESSION = 'CreateSession'
OKSPINOY_CREATE_SESSION_RESPONSE = 'CreateSessionResponse'
OKSPINOY_EXECUTE = 'Execute'
OKSPINOY_EXECUTE_RESPONSE = 'ExecuteResponse'


SOAP_ENVELOPE = """<?xml version="1.0" encoding="utf-8"?>
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Body>
{body}
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
"""

OKSPINOY_CREATE_SESSION_TPL = """<CreateSession xmlns="">
<username xmlns="">{username}</username>
</CreateSession>"""

OKSPINOY_EXECUTE_CMD_TPL = """<Execute xmlns="">
<SessionId xmlns="">{session_id}</SessionId>
<username xmlns="">{username}</username>
<password xmlns="">{password}</password>
<command xmlns="">{command}</command>
<data xmlns="">{data}</data>
</Execute>
"""

OKSPINOY_EXECUTE_CMD_DATA_TPL = """<?xml version="1.0" ?><meta>{xml_params}</meta>"""


### Example SOAP body ###
"""<CreateSession xmlns="">
<username xmlns="">Username</username>
</CreateSession>"""
OKSPINOY_CREATE_SESSION_DICT_TPL = {
    OKSPINOY_CREATE_SESSION: {
        XMLTODICT_XMLNS: "",
        USERNAME: ""
    }
}
