string_types = str, bytes
from .. import *
from ..exceptions import TransportError, HTTP_EXCEPTIONS
logger = logging.getLogger('okspinoy')


class Connection(object):
    """
    Class responsible for maintaining a connection to OksPinoy API. It
    holds persistent connection pool to it and it's main interface
    (`perform_request`) is thread-safe.
    Also responsible for logging.
    """
    transport_schema = 'http'

    def __init__(self, host='localhost', port=80, use_ssl=False, url_prefix='', timeout=10, **kwargs):
        """
        :arg host: hostname of the node (default: localhost)
        :arg port: port to use (integer, default: 9200)
        :arg url_prefix: optional url prefix for OksPinoy endpoint
        :arg timeout: default timeout in seconds (float, default: 10)
        """
        scheme = self.transport_schema
        if use_ssl:
            scheme += 's'
        self.host = '%s://%s:%s' % (scheme, host, port)
        if url_prefix:
            url_prefix = '/' + url_prefix.strip('/')
        self.url_prefix = url_prefix
        self.timeout = timeout
        self.logger = logger

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self.host)

    def _raise_error(self, status_code, raw_data):
        """ Locate appropriate exception and raise it. """
        error_message = raw_data
        additional_info = None
        try:
            if raw_data:
                additional_info = xmltodict.parse(raw_data)
                error_message = additional_info[SOAP_ENV_MAIN_TAG][SOAP_ENV_BODY_TAG]
                if isinstance(error_message, dict) and 'type' in error_message:
                    error_message = error_message['type']

        except (ValueError, TypeError) as err:
            self.logger.warning('Undecodable raw error response from server: %s', err)
        self.logger.warning((status_code, error_message, additional_info))

        raise HTTP_EXCEPTIONS.get(status_code, TransportError)(status_code, error_message, additional_info)