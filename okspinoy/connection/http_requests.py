try:
    import requests
    REQUESTS_AVAILABLE = True
except ImportError:
    REQUESTS_AVAILABLE = False

import re
from .base import *
from ..exceptions import *


class RequestsHttpConnection(Connection):
    # """
    # Connection using the `requests` library.
    # :arg http_auth: optional http auth information as either ':' separated
    #     string or a tuple. Any value will be passed into requests as `auth`.
    # :arg use_ssl: use ssl for the connection if `True`
    # :arg verify_certs: whether to verify SSL certificates
    # :arg ca_certs: optional path to CA bundle. By default standard requests'
    #     bundle will be used.
    # :arg client_cert: path to the file containing the private key and the
    #     certificate, or cert only if using client_key
    # :arg client_key: path to the file containing the private key if using
    #     separate cert and key files (client_cert will contain only the cert)
    # :arg headers: any custom http headers to be add to requests
    # """

    def __init__(self, host='localhost', port=80, http_auth=None,
                 use_ssl=True, verify_certs=True, ca_certs=None,
                 client_cert=None, client_key=None, headers=None, **kwargs):
        super(RequestsHttpConnection, self).__init__()
        if not REQUESTS_AVAILABLE:
            raise ConfigurationError("Please install requests to use RequestsHttpConnection.")

        super(RequestsHttpConnection, self).__init__(host=host, port=port, **kwargs)
        self.session = requests.Session()
        self.session.headers = headers or {}
        self.session.headers.setdefault('content-type', 'text/xml')
        if http_auth is not None:
            if isinstance(http_auth, (tuple, list)):
                http_auth = tuple(http_auth)
            elif isinstance(http_auth, string_types):
                http_auth = tuple(http_auth.split(':', 1))
            self.session.auth = http_auth

        self.base_url = 'http%s://%s:%d%s' % (
            's' if use_ssl else '',
            host, port, self.url_prefix
        )
        self.session.verify = verify_certs
        if not client_key:
            self.session.cert = client_cert
        elif client_cert:
            # cert is a tuple of (certfile, keyfile)
            self.session.cert = (client_cert, client_key)
        if ca_certs:
            if not verify_certs:
                raise ConfigurationError("You cannot pass CA certificates when verify SSL is off.")
            self.session.verify = ca_certs

        if use_ssl and not verify_certs:
            warnings.warn(
                'Connecting to %s using SSL with verify_certs=False is insecure.' % self.base_url)

    def perform_request(self, method, uri, params=None, body=None, headers=None, timeout=None, ignore=()):

        url = self.base_url + uri
        self.logger.debug((self, sys._getframe().f_code.co_name, url))
        if params:
            url = '%s?%s' % (url, urlencode(params or {}))

        start = time.time()
        try:
            self.logger.debug((self, sys._getframe().f_code.co_name, url, body, headers))
            request = requests.Request(method=method, url=url, data=body, headers=None)
            prepared_request = self.session.prepare_request(request)
            self.logger.debug((
                prepared_request.method,
                prepared_request.url,
                prepared_request.headers,
                prepared_request.body
            ))
            response = self.session.send(
                prepared_request,
                timeout=timeout or self.timeout)
            duration = time.time() - start
            raw_data = response.text
        except Exception as e:
            self.logger.warn((method, url, prepared_request.path_url, prepared_request.headers, body, time.time() - start, e, traceback.print_exc()))
            if isinstance(e, requests.exceptions.SSLError):
                raise SSLError('N/A', str(e), e)
            if isinstance(e, requests.Timeout):
                raise ConnectionTimeout('TIMEOUT', str(e), e)
            raise ConnectionError('N/A', str(e), e)

        # raise errors based on http status codes, let the client handle those if needed
        if not (200 <= response.status_code < 300) and response.status_code not in ignore:
            self.logger.warn((method, url, prepared_request.path_url, body, duration, response.status_code, raw_data))
            self._raise_error(response.status_code, raw_data)

        self.logger.debug(method, url, response.request.path_url, body, response.status_code, raw_data, duration)

        return response.status_code, response.headers, raw_data

    def close(self):
        """
        Explicitly closes connections
        """
        self.session.close()
